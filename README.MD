# Disroot's changelog

Here you can follow changes such as when service has been updated and to which version, when critical changes in the way service operates has been commited. We use this repository mainly to internally keep track of changes and to have better overview of the workloads. We did decided to share it publicly so anyone can follow on what we are busy with and keep track of what has been going on in the past. **Please do not submit issues to this repository!** We keep track of new versions of stofware. If you have issue, suggestion, feedback you would like to share with us, please use the general purpose board at https://git.disroot.org/Disroot/Disroot-Project 

## Deployments
We run deployments on bi-weekly basis on Tuesdays. We collect all proposed changes and updates over the week in form of issues in the repository and assign them to a milestone which in our case is the deployment date. We then test everything make sure changes are solid and ready to be pushed in production. Once all the work is done and ready, we run deployments, mark issues done and update our changelog.

