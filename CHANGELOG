---
title: Changelog
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: changelog
text_align: left
---

# ✨ CHANGELOG - Disroot.org ✨

### 11.05.2021 - #21
  - Updated Gitea to version 1.14.2
  - Updated Rainloop to verison 1.16.0
  - SSL Certificate renewal round
  - XMPP Prosody siskin/iOS push compatibility support (cloud_notify_filters, cloud_notify_priority_tag, cloud_notify_encrypted)
  - Dashboard update at https://apps.disroot.org

### 28.04.2021 - #20
  - Updated Cryptapad to version 4.4.0
  - Updated Gitea to version 1.14.1
  - (Email server) Increased  dovecot's imap notify interval to 25 minutes

### 13.04.2021 - #19
  - Updated Gitea to version 1.13.7
  - Updated PrivateBin to version 1.3.5
  - Updated Cryptpad to version 4.3.1
  - Update Akaunting to version 2.1.9
  - Migrated disroot.org and howto.disroot.org to new container

### 30.03.2021 - #18
  - Updated Gitea to version 1.13.6
  - Updated Cryptpad to version 4.3.0
  - Updated Rainloop to version 1.15.0
  - Updated SearX to version 1.0.0
  - Updated Etherpad to version 1.8.13

### 17.03.2021 - #17
  - Update Nextcloud to 20.0.8 together with number of apps
  - Update etherpad to 1.8.12
  - Update Lufi  to 0.05.13
  - Deploy updated ansible role for mailserver in production

### 19.02.2021 - #14
  - Update Cryptad using Ansible to version 4.1.0

### 09.01.2021 - #13
  - Renew some certificates
  - Nextcloud updates:
    - Nextcloud - 20.0.6
    - Deck

### 01.01.2021 - #12
  - Update Etherpad (0.18.7)
  - Nextcloud updates:
    - GitHub integration - 0.0.16
    - Notes - 4.0.2
    - News - 15.1.1
    - Cospend - 1.2.7
    - Disocurse integration - 0.0.6
    - Reddit integration - 0.0.9
    - Twitter integration - 0.0.7
  - Internal: roll out new db scripts

### 17.12.2020 - #11
  - Update Gitea (0.13.0)
  - Update Searx (0.18.0)
  - Update Lufi (0.05.11)
  - Update Etherpad (0.18.6)

### 11.09.20 - #09
  - Update Cryptpad
  - Renew certificates
  - Post the blogpost about deck
  - Deploy new backup scripts

### 14.08.20 - #06
  - Deploy Searx ansibled

### 07.08.20 - #05
  - Deploy proper Ansible on proxy01

### 12.07.22 - #04
  - Deploy devchat-converseJS
  - Deploy Matterbridge
  - Deploy properly all vhosts on proxy01
  - Update Diaspora

### 06.06.20 - #03
  - Update Discourse
  - Update Lufi
  - Update Matrix
  - Process custom domain linking
  - ~~Update Nextcloud~~ (postponed due to issues with deck and bookmakrs reported in last days on Github)
  - Update Zabbix

### 29.05.20 - #02
  - Update Cryptpad
  - Update Matrix/Riot
  - Deploy domain linking procedure
  - Deploy webmail farm setup
  - Nginx change ecliptic curve to support older devices
  - Cleanup discourse from all unused or abandoned categories

### 22.05.20 - #01
  - Ansible email role in production
  - Etherpad redeployment and update to 1.8.4
  - Deployment of Roundcube on cubetest.disroot.org and roundcube.disroot.org
  - Gitea update to 0.11.15
  - Update Akaunting
  - deploy Lufi beetroot theme

---
